<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index()
    {
        
        switch($_COOKIE['lang']) {
        case 'ru' : 
            $data = DB::table('products')->select(DB::raw("*, name_ru as name, description_ru as description"))->get();
        break;
        case 'uk' :
            $data = DB::table('products')->select(DB::raw("*, name_uk as name, description_uk as description"))->get();
        break;
        default: 
            case 'ru' : $data = DB::table('products')->select(DB::raw("*, name_ru as name, description_ru as description"))->get();
        break;
        }

        return view('home', [
            'data' => $data
        ]);
    }

    public function getProduct($id)
    {

        switch($_COOKIE['lang']) {
        case 'ru' : 
            $data = DB::table('products')->select(DB::raw("*, name_ru as name, description_ru as description"))->where('id', '=', $id)->get();
            $char = DB::table('characteristics')->select(DB::raw("*, name_ru as name, value_ru as value"))->where('product_id', '=', $id)->get();
        break;
        case 'uk' :
            $data = DB::table('products')->select(DB::raw("*, name_uk as name, description_uk as description"))->where('id', '=', $id)->get();
            $char = DB::table('characteristics')->select(DB::raw("*, name_uk as name, value_uk as value"))->where('product_id', '=', $id)->get();
        break;
        default: 
            $data = DB::table('products')->select(DB::raw("*, name_ru as name, description_ru as description"))->where('id', '=', $id)->get();
            $char = DB::table('characteristics')->select(DB::raw("*, name_ru as name, value_ru as value"))->where('product_id', '=', $id)->get();
        break;
        }

        return view('product', [
            'data' => $data,
            'char' => $char
        ]);
    }
}
