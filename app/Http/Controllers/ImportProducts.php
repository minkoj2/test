<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ImportProducts extends Controller
{
    public function import()
    {
        $json = file_get_contents('https://b2b-sandi.com.ua/api/products');

        $data = json_decode($json);

        for ($i=0; $i < count($data); $i++) {
            $id = DB::table('products')->insertGetId([
                'sku'            => $data[$i]->sku,
                'name_ru'        => $data[$i]->name_ru,
                'name_uk'        => $data[$i]->name_uk,
                'description_ru' => $data[$i]->description_ru,
                'description_uk' => $data[$i]->description_uk,
                'price'          => $data[$i]->price
            ]);
            
            for ($j=0; $j < count($data[$i]->characteristics); $j++) { 
                DB::table('characteristics')->insert([
                    'product_id' => $id,
                    'name_ru'    => $data[$i]->characteristics[$j]->name_ru,
                    'name_uk'    => $data[$i]->characteristics[$j]->name_uk,
                    'value_ru'   => $data[$i]->characteristics[$j]->value_ru,
                    'value_uk'   => $data[$i]->characteristics[$j]->value_uk
                ]);
            }
        }

        return redirect()->route('home');
    }
}
