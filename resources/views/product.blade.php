@include('inc.header')
        <main>
            <div class="container">
                <div class="row">
                    <div class="border w-100 p-2 m-0 row text-center">
                        <div class="col-sm-1">
                            <a href="{{route('home')}}" class="text-danger">←</a>
                        </div>
                        <div class="col-sm-4">
                            {{$data[0]->name}}
                        </div>
                        <div class="col-sm-4">
                            Артикуль: {{$data[0]->sku}}
                        </div>
                        <div class="col-sm-3">
                            Цена: {{$data[0]->price}}
                        </div>
                    </div>
                   <div class="border p-2">
                        Характеристики
                        <ul>
                            @foreach ($char as $item)
                                <li>{{$item->name}}: {{$item->value}}</li>
                            @endforeach
                        </ul>
                   </div>
                </div>
            </div>
        </main>
@include('inc.footer')
