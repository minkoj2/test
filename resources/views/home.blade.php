@include('inc.header')
        <main>
            <div class="container">
                <div class="row">
                    @foreach ($data as $item)
                    <div class="border w-50 p-2">
                        <p>{{$item->name}}</p>
                        <p>{{$item->description}}</p>
                        <div>Артикул: {{$item->sku}}</div>
                        <div class="text-success float-left">Цена: {{$item->price}}</div>
                        <a href="{{ route('product', $item->id) }}" class="float-right">Перейти к товару</a>
                    </div>
                    @endforeach
                </div>
            </div>
        </main>
@include('inc.footer')