<script>
    $("button[name='import'").on('click', function(){
        $.ajax({
            type: "post",
            url: "import",
            data: "_token={{ csrf_token() }}",
            success: function (response) {
                console.log('Success')
            }
        });
    })
    if($.cookie('lang') == undefined) {
        $.cookie('lang', 'ru')
    }
    $("button[name='lang']").on('click', function(){
        $.cookie('lang', $(this).val())
        document.location.href = document.location.href;
    })
</script>
</body>
</html>
